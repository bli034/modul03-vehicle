package vehicle;
import java.util.Calendar;

public class Bicycle extends Vehicle {
	private int gears;
	private Calendar productionDate;
	public Bicycle() {}
	public Bicycle(String name, String colour, int price, int model, String serialNumber, int direction, int gears) {
		super(name, colour, price, model, serialNumber, direction);
		this.gears = gears;
		productionDate = new java.util.GregorianCalendar();
	}
	
	@Override
	public void turnLeft(int degrees) {
		System.out.print("Bicycle turned " + degrees + "degrees left");
	}
	@Override
	public void turnRight(int degrees) {
		System.out.print("Bicycle turned " + degrees + "degrees right");
	}
	public int getGears() {
		return gears;
	}
	public void setGears(int gears) {
		this.gears = gears;
	}
	public Calendar getProductionDate() {
		return productionDate;
	}
	public void setProductionDate(Calendar date) {
		productionDate = date;
	}
	@Override
	public void setAllFields() {
		super.setAllFields();
		System.out.print("Gears: ");
		gears = input.nextInt();
		productionDate = new java.util.GregorianCalendar();
	}
	@Override
	public String toString() {
		return super.toString() + String.format(" Gears: %d Production date: %d-%d-%d", gears, productionDate.get(Calendar.YEAR), 
		    	   																			   productionDate.get(Calendar.MONTH), 
		    	   																			   productionDate.get(Calendar.DAY_OF_MONTH));
	}
}
