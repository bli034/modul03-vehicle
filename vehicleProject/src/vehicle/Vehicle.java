package vehicle;

public abstract class Vehicle {
	private String colour, name, serialNumber;
	private int model, price, direction;
	private double speed;
	protected java.util.Scanner input = new java.util.Scanner(System.in);
	public Vehicle() {}
	public Vehicle(String name, String colour, int price, int model, String serialNumber, int direction) {
		this.name = name;
		this.colour = colour;
		this.price = price;
		this.model = model;
		this.serialNumber = serialNumber;
		this.direction = direction;
		speed = 0;
	}
	public void setAllFields() {
		System.out.println("Input car data:");
		System.out.print("Name: ");
		name = input.next();
		System.out.print("Colour: ");
		colour = input.next();
		System.out.print("Price: ");
		price = input.nextInt();
		System.out.print("Model: ");
		model = input.nextInt();
		System.out.print("Serial #: ");
		serialNumber = input.next();
		direction = 0;
		speed = 0;
	}
	public abstract void turnLeft(int degrees);
	public abstract void turnRight(int degrees);
	public String getColour() {
		return colour;
	}
	public void setColour(String colour) {
		this.colour = colour;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public int getModel() {
		return model;
	}
	public void setModel(int model) {
		this.model = model;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getDirection() {
		return direction;
	}
	public void setDirection(int direction) {
		this.direction = direction;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public String toString() {
		return String.format("Name: %s Colour: %s Price: %d Model: %d Serial#: %s Direction: %d Speed: %f", name, colour, price, model, serialNumber, direction, speed);
	}
}