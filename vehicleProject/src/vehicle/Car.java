package vehicle;
import java.util.Calendar;

public class Car extends Vehicle {
	private int power;
	private Calendar productionDate;
	public Car() {}
	public Car(String name, String colour, int price, int model, String serialNumber, int direction, int power) {
		super(name, colour, price, model, serialNumber, direction);
		this.power = power;
		productionDate = new java.util.GregorianCalendar();
	}
	@Override
	public void setAllFields() {
		super.setAllFields();
		System.out.print("Power: ");
		power = input.nextInt();
		productionDate = new java.util.GregorianCalendar();
	}
	@Override
	public void turnLeft(int degrees) {
		if (degrees >= 0 && degrees <= 360) {
			super.setDirection((super.getDirection() - degrees + 360) % 360);
		}
	}
	@Override
	public void turnRight(int degrees) {
		if (degrees >= 0 && degrees <= 360) {
			super.setDirection((super.getDirection() + degrees) % 360);
		}
	}
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	public Calendar getProductionDate() {
		return productionDate;
	}
	public void setProductionDate(Calendar date) {
		productionDate = date;
	}
	@Override
	public String toString() {
		return super.toString() + String.format(" Power: %d Production date: %d-%d-%d", power, productionDate.get(Calendar.YEAR), 
																					    	   productionDate.get(Calendar.MONTH), 
																					    	   productionDate.get(Calendar.DAY_OF_MONTH));
	}
}
